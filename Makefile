.PHONY: install

pkg ?= "org.bjjb.webostv_0.0.1_all.ipk"
device ?= "tv"

$(pkg):
	ares-package .

install: $(pkg)
	ares-install -d $(device) $(pkg)
